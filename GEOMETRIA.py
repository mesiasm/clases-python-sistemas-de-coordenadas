"""                author: "Angel Morocho"
             email: "angel.m.morocho.c@unl.edu.ec"

Ejercicio 01:
             >> CREE  UN MÓDULO PYTHON  LLAMADO "geometria" Y EN EL REALICE LO SIGUIENTE:
             >> Crear una clase llamada Punto con sus dos coordenadas X e Y.
             >> Añadir un método constructor para crear puntos fácilmente. Si no se reciben una coordenada,
                su valor será cero.
             >> Sobreescribir el método __str__, para que al imprimir por pantalla un punto aparezca en formato (X,Y)
             >> Añadir un método llamado cuadrante que indique a qué cuadrante pertenece el punto, teniendo en cuenta
                que si X == 0 e Y != 0 se sitúa sobre el eje Y, si X != 0 e Y == 0 se sitúa sobre el eje X y si X == 0
                Y == 0 está sobre el origen.
             >> Añadir un método llamado vector, que tome otro punto como parámetro y calcule el vector resultante
                entre los dos puntos.
             >> Añadr un método llamado distancia, que tome otro punto como parámetro y calcule la distancia entre
                los dos puntos y la devuelva. Recuere la fórmula:
             >> Crear una clase llamada Rectangulo con dos puntos (inicial y final) que formarán la diagonal del
                 rectángulo.
             >> Añadir un método constructor para crear ambos puntos fácilmente, si no se envían se crearán dos
                 puntos en el origen por defecto.
             >> Añadir al rectángulo un método llamado base que devuelva la base.
             >> Añade al rectángulo un método llamado altura que devuelva la altura.
             >> Añade al rectángulo un método llamado area que devuelva el area."""

import math


class Punto:

    def __init__(self, x = 0, y = 0):
        self.x = x
        self.y = y

    def __str__(self):
        return "({}, {})".format(self.x, self.y)

    def cuadrante(self):
        if self.x > 0 and self.y > 0:
            print("{} pertenece al primer cuadrante".format(self))

        elif self.x < 0 and self.y > 0:
            print("{} pertenece al segundo cuadrante".format(self))

        elif self.x < 0 and self.y < 0:
            print("{} pertenece al tercer cuadrante".format(self))

        elif self.x > 0 and self.y < 0:
            print("{} pertenece al cuarto cuadrante".format(self))

        elif self.x != 0 and self.y == 0:
            print("{} se sitúa sobre el eje X".format(self))

        elif self.x == 0 and self.y != 0:
            print("{} se sitúa sobre el eje Y".format(self))

        else:
            print("{} se encuentra sobre el origen".format(self))

    def vector(self, p):
        print("El vector entre {} y {} es ({}, {})".format(
            self, p, p.x - self.x, p.y - self.y) )

    def distancia(self, p):
        d = math.sqrt((p.x - self.x)**2 + (p.y - self.y)** 2)
        print("La distancia entre los puntos {} y {} es {}".format(self, p, d))


class Rectangulo:

    def __init__(self, pInicial=Punto(), pFinal=Punto()):
        self.pInicial = pInicial
        self.pFinal = pFinal

        # Hago los cálculos, pero no llamo los atributos igual
        # que los métodos porque sino podríamos sobreescribirlos

        self.vBase = abs(self.pFinal.x - self.pInicial.x)
        self.vAltura = abs(self.pFinal.y - self.pInicial.y)
        self.vArea = self.vBase * self.vAltura

    def base(self):
        print("La base del rectángulo es {}".format( self.vBase ) )

    def altura(self):
        print("La altura del rectángulo es {}".format( self.vAltura ) )

    def area(self):
        print("El área del rectángulo es {}".format( self.vArea ) )


"""Creación los puntos A(2, 3), B(5,5), C(-3, -1) y D(0,0)"""
A = Punto(2, 3)
B = Punto(5, 5)
C = Punto(-3, -1)
D = Punto(0, 0)

print(f'El punto A es igual a: {A}')
print(f'El punto B es igual a: {B}')
print(f'El punto C es igual a: {C}')
print(f'El punto D es igual a: {D}\n')


"""Consulta a que cuadrante pertenecen el punto A, C y D \n"""
A.cuadrante()
C.cuadrante()
D.cuadrante()


"""Consultar la distancia entre los puntos 'A y B' y 'B y A \n"""
A.distancia(B)
B.distancia(A)


"""Determina cuales de los 3 puntos A, B o C, se encuentra más lejos del origen, punto (0,0) \n"""
A.distancia(D)
B.distancia(D)
C.distancia(D)

R = Rectangulo(A, B)
R.base()
R.altura()
R.area()